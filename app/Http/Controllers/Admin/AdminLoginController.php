<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminLoginController extends Controller
{
    //Amdin Login
    public function adminLogin()
    {
        if (Auth::guard('admin')->check()) {
        } else {
            return view('admin.auth.login');
        }
    }

    public function loginAdmin(Request $request){
        $data = $request->all();
        $validateData =$request->validate([
            'email' => 'required|email|max:255',
            'password' =>'required'
        ]);
        if(Auth::guard('admin') ->attempt(['email' => $data['email'], 'password' => $data['password']])) {
            return redirect('/admin/dashboard');
        } else {
            Session::flash('error_message', 'Invalid Email or Password');
            return redirect()->back();
        }
    }

    //Admin Dashboard
    public function adminDashboard(){
        return view('admin.dashboard');
    }

    //Admin Logout
    public function adminLogout(){
        Auth::guard('admin')->logout();
        Session::flash('info_message', 'Logout Successful');
        return redirect('/admin/login');
    }
}



