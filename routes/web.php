<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', [App\Http\Controllers\Front\IndexController::class, 'index'])->name('index');

Route::prefix('/admin')->group(function (){
//Admin Login
Route::get('/login', [\App\Http\Controllers\Admin\AdminLoginController::class,'adminLogin'])->name('adminLogin');
Route::post('/login', [\App\Http\Controllers\Admin\AdminLoginController::class,'loginAdmin'])->name('loginAdmin');
//Admin Dashboard
Route::group(['middleware' => 'admin'], function (){
    Route::get('/dashboard', [\App\Http\Controllers\Admin\AdminLoginController::class,'adminDashboard'])->name('adminDashboard');
    //Admin Profile
    Route::get('/profile', [\App\Http\Controllers\Admin\AdminProfileController::class,'adminProfile'])->name('adminProfile');
    //Admin Profile Update
    Route::post('/profile/update/{id}', [\App\Http\Controllers\Admin\AdminProfileController::class,'adminProfileUpdate'])->name('adminProfileUpdate');

    // Delete Image
    Route::get('/delete-image/{id}', [\App\Http\Controllers\Admin\AdminProfileController::class, 'deleteImage'])->name('deleteImage');

// Change Password
    Route::get('/change-password', [\App\Http\Controllers\Admin\AdminProfileController::class, 'changePassword'])->name('changePassword');

    // Check Password
    Route::post('/check-password', [\App\Http\Controllers\Admin\AdminProfileController::class, 'chkUserPassword'])->name('chkUserPassword');


});
    Route::get('logout',[\App\Http\Controllers\Admin\AdminLoginController::class,'adminLogout'])->name('adminLogout');

});


